package ru.bokhan.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.dto.TaskDto;
import ru.bokhan.tm.repository.dto.TaskDtoRepository;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class TaskEndpoint {

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @NotNull
    @WebMethod
    public List<TaskDto> findAll() {
        return taskDtoRepository.findAll();
    }

    @NotNull
    @WebMethod
    public TaskDto save(@NotNull @WebParam(name = "task") final TaskDto task) {
        return taskDtoRepository.save(task);
    }

    @Nullable
    @WebMethod
    public TaskDto findById(@NotNull @WebParam(name = "id") final String id) {
        return taskDtoRepository.findById(id).orElse(null);
    }

    @WebMethod
    public boolean existsById(@NotNull @WebParam(name = "id") final String id) {
        return taskDtoRepository.existsById(id);
    }

    @WebMethod
    public long count() {
        return taskDtoRepository.count();
    }

    @WebMethod
    public void deleteById(@NotNull @WebParam(name = "id") final String id) {
        taskDtoRepository.deleteById(id);
    }

    @WebMethod
    public void deleteAll() {
        taskDtoRepository.deleteAll();
    }

}

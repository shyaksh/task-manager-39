package ru.bokhan.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.bokhan.tm.api.endpoint.IProjectRestEndpoint;
import ru.bokhan.tm.dto.ProjectDto;
import ru.bokhan.tm.repository.dto.ProjectDtoRepository;
import ru.bokhan.tm.repository.entity.ProjectRepository;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Override
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDto save(@NotNull @RequestBody final ProjectDto s) {
        return projectDtoRepository.save(s);
    }

    @Nullable
    @Override
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDto findById(@NotNull @PathVariable("id") final String id) {
        return projectDtoRepository.findById(id).orElse(null);
    }

    @Override
    @GetMapping(value = "/exists/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectDtoRepository.existsById(id);
    }

    @Override
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteById(@NotNull @PathVariable("id") final String id) {
        projectRepository.deleteById(id);
    }

}

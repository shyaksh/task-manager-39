package ru.bokhan.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bokhan.tm.dto.ProjectDto;
import ru.bokhan.tm.repository.dto.ProjectDtoRepository;
import ru.bokhan.tm.repository.entity.ProjectRepository;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class ProjectEndpoint {

    @Autowired
    private ProjectDtoRepository projectDtoRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @WebMethod
    public List<ProjectDto> findAll() {
        return projectDtoRepository.findAll();
    }

    @NotNull
    @WebMethod
    public ProjectDto save(@NotNull @WebParam(name = "project") final ProjectDto project) {
        return projectDtoRepository.save(project);
    }

    @Nullable
    @WebMethod
    public ProjectDto findById(@NotNull @WebParam(name = "id") final String id) {
        return projectDtoRepository.findById(id).orElse(null);
    }

    @WebMethod
    public boolean existsById(@NotNull @WebParam(name = "id") final String id) {
        return projectDtoRepository.existsById(id);
    }

    @WebMethod
    public long count() {
        return projectDtoRepository.count();
    }

    @WebMethod
    public void deleteById(@NotNull @WebParam(name = "id") final String id) {
        projectRepository.deleteById(id);
    }

    @WebMethod
    public void deleteAll() {
        projectRepository.deleteAll();
    }

}

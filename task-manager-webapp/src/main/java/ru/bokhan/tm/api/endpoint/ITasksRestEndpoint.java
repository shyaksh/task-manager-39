package ru.bokhan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.bokhan.tm.dto.TaskDto;

import java.util.List;

@RequestMapping("/api/tasks")
public interface ITasksRestEndpoint {

    @NotNull
    @GetMapping
    List<TaskDto> findAll();

    @NotNull
    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    List<TaskDto> saveAll(@NotNull final List<TaskDto> list);

    @NotNull
    @GetMapping("/count")
    Long count();

    @DeleteMapping
    void deleteAll(@NotNull final List<TaskDto> list);

    @DeleteMapping("/all")
    void deleteAll();

}

package ru.bokhan.tm.api.endpoint;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.bokhan.tm.dto.ProjectDto;

import java.util.List;

@RequestMapping("/api/projects")
public interface IProjectsRestEndpoint {

    @NotNull
    static IProjectsRestEndpoint client(@NotNull final String baseUrl) {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(IProjectsRestEndpoint.class, baseUrl);
    }

    @NotNull
    @GetMapping
    List<ProjectDto> findAll();

    @NotNull
    @RequestMapping(method = {RequestMethod.POST})
    List<ProjectDto> saveAll(@NotNull final List<ProjectDto> list);

    @NotNull
    @GetMapping("/count")
    Long count();

    @DeleteMapping
    void deleteAll(@NotNull final List<ProjectDto> list);

    @DeleteMapping("/all")
    void deleteAll();

}

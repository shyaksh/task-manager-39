package ru.bokhan.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class UserDTO extends AbstractEntityDTO {

    public static final long serialVersionUID = 1L;

    @Column
    @NotNull
    private String login;

    @Column
    @NotNull
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Column
    @Nullable
    private String firstName;

    @Column
    @Nullable
    private String lastName;

    @Column
    @Nullable
    private String middleName;

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;

    @Column
    @NotNull
    private Boolean locked = false;

    public UserDTO(
            @NotNull String id,
            @NotNull String login,
            @NotNull String passwordHash,
            @Nullable String email,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName,
            @Nullable Role role,
            @NotNull Boolean locked
    ) {
        super(id);
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.role = role;
        this.locked = locked;
    }

}
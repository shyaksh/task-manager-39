package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.api.service.IUserService;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.empty.*;
import ru.bokhan.tm.exception.incorrect.IncorrectIdException;
import ru.bokhan.tm.repository.dto.UserRepositoryDTO;
import ru.bokhan.tm.repository.entity.UserRepository;
import ru.bokhan.tm.util.HashUtil;

@Service
public class UserService extends AbstractService<UserDTO, User> implements IUserService {

    @NotNull
    @Autowired
    private UserRepositoryDTO dtoRepository;

    @NotNull
    @Autowired
    private UserRepository repository;

    @Nullable
    @Override
    public UserDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return dtoRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return dtoRepository.findByLogin(login);
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.deleteById(id);
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        save(user);
    }

    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        save(user);
    }

    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        user.setRole(role);
        save(user);
    }

    @Override
    public void updateById(
            @Nullable final String id, @Nullable final String login,
            @Nullable final String firstName, @Nullable final String lastName,
            @Nullable final String middleName,
            @Nullable final String email
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final UserDTO userDTO = findById(id);
        if (userDTO == null) throw new IncorrectIdException();
        userDTO.setLogin(login);
        userDTO.setFirstName(firstName);
        userDTO.setMiddleName(middleName);
        userDTO.setLastName(lastName);
        userDTO.setEmail(email);
        save(userDTO);
    }

    @Override
    public void updatePasswordById(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final UserDTO user = findById(id);
        if (user == null) throw new IncorrectIdException();
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        save(user);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return;
        user.setLocked(true);
        save(user);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return;
        user.setLocked(false);
        save(user);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        repository.deleteByLogin(login);
    }

}

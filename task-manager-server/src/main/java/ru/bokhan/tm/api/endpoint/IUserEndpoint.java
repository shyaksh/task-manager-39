package ru.bokhan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IUserEndpoint {

    @WebMethod
    void createUser(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    );

    @WebMethod
    void createUserWithEmail(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password,
            @WebParam(name = "email") @Nullable String email
    );

    @WebMethod
    void registerUserWithEmail(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password,
            @WebParam(name = "email") @Nullable String email
    );

    @WebMethod
    void createUserWithRole(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password,
            @WebParam(name = "role") @Nullable Role role
    );

    @Nullable
    @WebMethod
    UserDTO findUserById(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "id") @Nullable String id
    );

    @Nullable
    @WebMethod
    UserDTO findUserByLogin(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login
    );

    @WebMethod
    void removeUserById(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "id") @Nullable String id
    );

    @WebMethod
    void removeUserByLogin(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login
    );

    @WebMethod
    void updateUser(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "firstName") @Nullable String firstName,
            @WebParam(name = "lastName") @Nullable String lastName,
            @WebParam(name = "middleName") @Nullable String middleName,
            @WebParam(name = "email") @Nullable String email
    );

    @WebMethod
    void updateUserById(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "firstName") @Nullable String firstName,
            @WebParam(name = "lastName") @Nullable String lastName,
            @WebParam(name = "middleName") @Nullable String middleName,
            @WebParam(name = "email") @Nullable String email
    );

    @WebMethod
    void updateUserPassword(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "password") @Nullable String password
    );

    @WebMethod
    void updateUserPasswordById(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "id") @Nullable String id,
            @WebParam(name = "password") @Nullable String password
    );

    @WebMethod
    void lockUserByLogin(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login
    );

    @WebMethod
    void unlockUserByLogin(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login
    );

    @NotNull
    @WebMethod
    List<UserDTO> findUserAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    void removeUserAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    );

    @WebMethod
    void removeUser(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "user") @Nullable UserDTO user
    );

}
